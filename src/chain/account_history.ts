/**
 * @file Blurt type definitions related to Account History.
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @description adaptation from Johan Nordberg <code@johan-nordberg.com> type definitions related to operation.
 * @license
 * Copyright (c) 2017 Johan Nordberg. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistribution of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *  2. Redistribution in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  3. Neither the name of the copyright holder nor the names of its contributors
 *     may be used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You acknowledge that this software is not designed, licensed or intended for use
 * in the design, construction, operation or maintenance of any military facility.
 */
import { PublicKey } from '../crypto'
import { AuthorityType } from './account'
import { BeneficiaryRoute } from './comment'
import { WitnessUpdateProperties, HexBuffer } from './misc'

/**
 * Operation type.
 * Ref: https://gitlab.com/blurt/blurt/-/blob/dev/libraries/protocol/include/blurt/protocol/operations.hpp
 */
export type OperationType =  // ordered by <id>
  | 'vote_operation' // 0
  | 'comment_operation' // 1

  | 'transfer_operation' // 2
  | 'transfer_to_vesting_operation' // 3
  | 'withdraw_vesting_operation' // 4

  | 'account_create_operation' // 5
  | 'account_update_operation' // 6

  | 'witness_update_operation' // 7
  | 'account_witness_vote_operation' // 8
  | 'account_witness_proxy_operation' // 9

  | 'custom_operation' // 10

  | 'delete_comment_operation' // 11
  | 'custom_json_operation' // 12
  | 'comment_options_operation' // 13
  | 'set_withdraw_vesting_route_operation' // 14
  | 'claim_account_operation' // 15
  | 'create_claimed_account_operation' // 16
  | 'request_account_recovery_operation' // 17
  | 'recover_account_operation' // 18
  | 'change_recovery_account_operation' // 19
  | 'escrow_transfer_operation' // 20
  | 'escrow_dispute_operation' // 21
  | 'escrow_release_operation' // 22
  | 'escrow_approve_operation' // 23
  | 'transfer_to_savings_operation' // 24
  | 'transfer_from_savings_operation' // 25
  | 'cancel_transfer_from_savings_operation' // 26
  | 'custom_binary_operation' // 27
  | 'decline_voting_rights_operation' // 28
  | 'reset_account_operation' // 29
  | 'set_reset_account_operation' // 30
  | 'claim_reward_balance_operation' // 31
  | 'delegate_vesting_shares_operation' // 32
  | 'witness_set_properties_operation' // 33
  | 'create_proposal_operation' // 34
  | 'update_proposal_votes_operation' // 35
  | 'remove_proposal_operation' // 36

export const OperationTypeRegexp = new RegExp(
  '^vote_operation$'
  + '|^comment_operation$'
  + '|^transfer_operation$'
  + '|^transfer_to_vesting_operation$'
  + '|^withdraw_vesting_operation$'
  + '|^account_create_operation$'
  + '|^account_update_operation$'
  + '|^witness_update_operation$'
  + '|^account_witness_vote_operation$'
  + '|^account_witness_proxy_operation$'
  + '|^custom_operation$'
  + '|^delete_comment_operation$'
  + '|^custom_json_operation$'
  + '|^comment_options_operation$'
  + '|^set_withdraw_vesting_route_operation$'
  + '|^claim_account_operation$'
  + '|^create_claimed_account_operation$'
  + '|^request_account_recovery_operation$'
  + '|^recover_account_operation$'
  + '|^change_recovery_account_operation$'
  + '|^escrow_transfer_operation$'
  + '|^escrow_dispute_operation$'
  + '|^escrow_release_operation$'
  + '|^escrow_approve_operation$'
  + '|^transfer_to_savings_operation$'
  + '|^transfer_from_savings_operation$'
  + '|^cancel_transfer_from_savings_operation$'
  + '|^custom_binary_operation$'
  + '|^decline_voting_rights_operation$'
  + '|^reset_account_operation$'
  + '|^set_reset_account_operation$'
  + '|^claim_reward_balance_operation$'
  + '|^delegate_vesting_shares_operation$'
  + '|^witness_set_properties_operation$'
  + '|^create_proposal_operation$'
  + '|^update_proposal_votes_operation$'
  + '|^remove_proposal_operation$'
)

/**
 * Virtual operation type.
 */
 export type VirtualOperationType =  // ordered by <id>
  | 'author_reward_operation' // 37
  | 'curation_reward_operation' // 38
  | 'comment_reward_operation' // 39
  | 'fill_vesting_withdraw_operation' // 40
  | 'shutdown_witness_operation' // 41
  | 'fill_transfer_from_savings_operation' // 42
  | 'hardfork_operation' // 43
  | 'comment_payout_update_operation' // 44
  | 'return_vesting_delegation_operation' // 45
  | 'comment_benefactor_reward_operation' // 46
  | 'producer_reward_operation' // 47
  | 'clear_null_account_balance_operation' // 48
  | 'proposal_pay_operation' // 49
  | 'sps_fund_operation' // 50
  | 'fee_pay_operation' // 51

export const VirtualOperationTypeRegexp = new RegExp(
  '^author_reward_operation$'
  + '|^curation_reward_operation$'
  + '|^comment_reward_operation$'
  + '|^fill_vesting_withdraw_operation$'
  + '|^shutdown_witness_operation$'
  + '|^fill_transfer_from_savings_operation$'
  + '|^hardfork_operation$'
  + '|^comment_payout_update_operation$'
  + '|^return_vesting_delegation_operation$'
  + '|^comment_benefactor_reward_operation$'
  + '|^producer_reward_operation$'
  + '|^clear_null_account_balance_operation$'
  + '|^proposal_pay_operation$'
  + '|^sps_fund_operation$'
  + '|^fee_pay_operation$'
)

/**  Applied operation. */
export interface AppliedOperation {
  trx_id:       string
  block:        number
  trx_in_block: number
  op_in_trx:    number
  virtual_op:   number
  timestamp:    string
  op: {
    type:  OperationType | VirtualOperationType
    value: { [key: string]: any }
  }
}

/** Account Create. */
export interface AppliedAccountCreateOperation extends AppliedOperation {
  op: {
    type: 'account_create_operation'
    value: {
      fee: {
        amount:    string
        precision: number
        nai:       string
      }
      creator:          string // account_name_type
      new_account_name: string // account_name_type
      owner:            AuthorityType
      active:           AuthorityType
      posting:          AuthorityType
      memo_key:         string | PublicKey // public_key_type
      json_metadata:    string
    }
  }
}

/** Account Update. */
export interface AppliedAccountUpdateOperation extends AppliedOperation {
  op: {
    type: 'account_update_operation'
    value: {
      account:               string
      owner?:                AuthorityType
      active?:               AuthorityType
      posting?:              AuthorityType
      memo_key?:             string | PublicKey
      json_metadata:         string | ''
      posting_json_metadata: string | ''
      extensions:            any[]
    }
  }
}

export interface AppliedAccountWitnessProxyOperation extends AppliedOperation {
  op: {
    type: 'account_witness_proxy_operation'
    value: {
      account: string // account_name_type
      proxy:   string // account_name_type
    }
  }
}

/** Vote/Unvote for a Witness. */
export interface AppliedAccountWitnessVoteOperation extends AppliedOperation {
  op: {
    type: 'account_witness_vote_operation'
    value: {
      account: string
      witness: string
      approve: boolean
    }
  }
}

/** Author receive reward for his content. */
export interface AppliedAuthorRewardOperation extends AppliedOperation {
  op: {
    type: 'author_reward_operation'
    value: {
      author:   string // account_name_type
      permlink: string
      blurt_payout: {
        amount:    string
        precision: number
        nai:       string
      }
      vesting_payout: {
        amount:    string
        precision: number
        nai:       string
      }
    }
  }
}

export interface AppliedCancelTransferFromSavingsOperation extends AppliedOperation {
  op: {
    type: 'cancel_transfer_from_savings_operation'
    value: {
      from:       string // account_name_type
      request_id: number // uint32_t
    }
  }
}

export interface AppliedChangeRecoveryAccountOperation extends AppliedOperation {
  op: {
    type: 'change_recovery_account_operation'
    value: {
      /** The account that would be recovered in case of compromise. */
      account_to_recover: string // account_name_type
      /** The account that creates the recover request. */
      new_recovery_account: string // account_name_type
      /** Extensions. Not currently used. */
      extensions: any[] // extensions_type
    }
  }
}

export interface AppliedClaimAccountOperation extends AppliedOperation {
  op: {
    type: 'claim_account_operation'
    value: {
      creator: string // account_name_type
      fee: {
        amount:    string
        precision: number
        nai:       string
      }
      /** Extensions. Not currently used. */
      extensions: any[] // extensions_type
    }
  }
}

export interface AppliedClaimRewardBalanceOperation extends AppliedOperation {
  op: {
    type: 'claim_reward_balance_operation'
    value: {
      account: string // account_name_type
      reward_blurt: {
        amount:    string
        precision: number
        nai:       string
      }
      reward_vests: {
        amount:    string
        precision: number
        nai:       string
      }
    }
  }
}

export interface AppliedCommentBenefactorRewardOperation extends AppliedOperation {
  op: {
    type: 'comment_benefactor_reward_operation'
    value: {
      benefactor:     string // account_name_type
      author:         string // account_name_type
      permlink:       string
      blurt_payout:{
        amount:    string
        precision: number
        nai:       string
      }
      vesting_payout: {
        amount:    string
        precision: number
        nai:       string
      }
    }
  }
}

export interface AppliedCommentOperation extends AppliedOperation {
  op: {
    type: 'comment_operation'
    value: {
      /** the author that comment is being submitted to, when posting a new blog this is an empty string. */
      parent_author: string // account_name_type
      /** specific post that comment is being submitted to, when posting a new blog this become the category of the post (tags[0]). */
      parent_permlink: string
      /** author of the post/comment being submitted (account name). */
      author: string // account_name_type
      /** unique string identifier for the post, linked to the author of the post. */
      permlink: string
      /** human readable title of the post being submitted, this is often blank when commenting. */
      title: string
      /** body of the post/comment being submitted, or diff-match-patch when updating. */
      body: string
      /** JSON object string. */
      json_metadata: string
    }
  }
}

export interface AppliedCommentOptionsOperation extends AppliedOperation {
  op: {
    type: 'comment_options_operation'
    value: {
      /** author of the post/comment being submitted (account name). */
      author: string // account_name_type
      /** human readable title of the post being submitted, this is often blank when commenting. */
      permlink: string
      /** the maximum payout this post will receive. asset( 1000000000, BLURT_SYMBOL ) */
      max_accepted_payout: {
        amount:    string
        precision: number
        nai:       string
      }
      /** The percent of BLURT to key, unkept amounts will be received as BLURT Power. */
      // percent_blurt: number
      /** Whether to allow post to receive votes. */
      allow_votes: boolean
      /** Whether to allow post to recieve curation rewards. If false rewards return to reward fund. */
      allow_curation_rewards: boolean
      extensions: [
        number,
        /** Must be specified in sorted order (account ascending; no duplicates) */
        { beneficiaries: BeneficiaryRoute[] } | { percent_blurt: number }][] // flat_set< comment_options_extension >
    }
  }
}

export interface AppliedCreateClaimedAccountOperation extends AppliedOperation {
  op: {
    type: 'create_claimed_account_operation'
    value: {
      creator:          string // account_name_type
      new_account_name: string // account_name_type
      owner:            AuthorityType
      active:           AuthorityType
      posting:          AuthorityType
      memo_key:         string | PublicKey // public_key_type
      json_metadata:    string
      /** Extensions. Not currently used. */
      extensions: any[] // extensions_type
    }
  }
}

export interface AppliedCreateProposalOperation extends AppliedOperation {
  op: {
    type: 'create_proposal_operation'
    value: {
      creator:    string
      receiver:   string
      start_date: string // time_point_sec
      end_date:   string // time_point_sec
      daily_pay: {
        amount:    string
        precision: number
        nai:       string
      }
      subject:  string
      permlink: string
      /** Extensions. Not currently used. */
      extensions: any[] // extensions_type
    }
  }
}

/** Account receive curation reward for his upvote. */
export interface AppliedCurationRewardOperation extends AppliedOperation {
  op: {
    type: 'curation_reward_operation'
    value: {
      curator: string // account_name_type
      reward:{
        amount:    string
        precision: number
        nai:       string
      } // VESTS
      comment_author:   string // account_name_type
      comment_permlink: string
    }
  }
}

/**
 * @brief provides a generic way to add higher level protocols on top of witness consensus
 * @ingroup operations
 *
 * There is no validation for this operation other than that required auths are valid
 */
export interface AppliedCustomOperation extends AppliedOperation {
  op: {
    type: 'custom_operation'
    value: {
      required_auths: string[]
      id: number // uint16
      data: Buffer | HexBuffer | number[]
    }
  }
}

export interface AppliedCustomJsonOperation extends AppliedOperation {
  op: {
    type: 'custom_json_operation'
    value: {
      required_auths: string[] // flat_set< account_name_type >
      required_posting_auths: string[] // flat_set< account_name_type >
      /** ID string, must be less than 32 characters long. */
      id: string
      /** JSON encoded string, must be valid JSON. */
      json: string
    }
  }
}

export interface AppliedDeclineVotingRightsOperation extends AppliedOperation {
  op: {
    type: 'decline_voting_rights_operation'
    value: {
      account: string // account_name_type
      decline: boolean
    }
  }
}

export interface AppliedDelegateVestingSharesOperation extends AppliedOperation {
  op: {
    type: 'delegate_vesting_shares_operation'
    value: {
      /** The account delegating vesting shares. */
      delegator: string // account_name_type
      /** The account receiving vesting shares. */
      delegatee: string // account_name_type
      /** The amount of vesting shares delegated. */
      vesting_shares: {
        amount:    string
        precision: number
        nai:       string
      }
    }
  }
}

export interface AppliedDeleteCommentOperation extends AppliedOperation {
  op: {
    type: 'delete_comment_operation'
    value: {
      author: string // account_name_type
      permlink: string
    }
  }
}

/**
 * The agent and to accounts must approve an escrow transaction for it to be valid on
 * the blockchain. Once a part approves the escrow, the cannot revoke their approval.
 * Subsequent escrow approve operations, regardless of the approval, will be rejected.
 */
 export interface AppliedEscrowApproveOperation extends AppliedOperation {
  op: {
    type: 'escrow_approve_operation'
    value: {
      from: string // account_name_type
      to: string // account_name_type
      agent: string // account_name_type
      /** Either to or agent. */
      who: string // account_name_type
      escrow_id: number // uint32_t
      approve: boolean
    }
  }
}

/**
 * If either the sender or receiver of an escrow payment has an issue, they can
 * raise it for dispute. Once a payment is in dispute, the agent has authority over
 * who gets what.
 */
 export interface AppliedEscrowDisputeOperation extends AppliedOperation {
  op: {
    type: 'escrow_dispute_operation'
    value: {
      from: string // account_name_type
      to: string // account_name_type
      agent: string // account_name_type
      who: string // account_name_type
      escrow_id: number // uint32_t
    }
  }
}

/**
 * This operation can be used by anyone associated with the escrow transfer to
 * release funds if they have permission.
 *
 * The permission scheme is as follows:
 * If there is no dispute and escrow has not expired, either party can release funds to the other.
 * If escrow expires and there is no dispute, either party can release funds to either party.
 * If there is a dispute regardless of expiration, the agent can release funds to either party
 *    following whichever agreement was in place between the parties.
 */
 export interface AppliedEscrowReleaseOperation extends AppliedOperation {
  op: {
    type: 'escrow_release_operation'
    value: {
      from: string // account_name_type
      /** The original 'to'. */
      to: string // account_name_type
      agent: string // account_name_type
      /** The account that is attempting to release the funds, determines valid 'receiver'. */
      who: string // account_name_type
      /** The account that should receive funds (might be from, might be to). */
      receiver: string // account_name_type
      escrow_id: number // uint32_t
      /** The amount of blurt to release. */
      blurt_amount: {
        amount:    string
        precision: number
        nai:       string
      }
    }
  }
}

/**
 * The purpose of this operation is to enable someone to send money contingently to
 * another individual. The funds leave the *from* account and go into a temporary balance
 * where they are held until *from* releases it to *to* or *to* refunds it to *from*.
 *
 * In the event of a dispute the *agent* can divide the funds between the to/from account.
 * Disputes can be raised any time before or on the dispute deadline time, after the escrow
 * has been approved by all parties.
 *
 * This operation only creates a proposed escrow transfer. Both the *agent* and *to* must
 * agree to the terms of the arrangement by approving the escrow.
 *
 * The escrow agent is paid the fee on approval of all parties. It is up to the escrow agent
 * to determine the fee.
 *
 * Escrow transactions are uniquely identified by 'from' and 'escrow_id', the 'escrow_id' is defined
 * by the sender.
 */
 export interface AppliedEscrowTransferOperation extends AppliedOperation {
  op: {
    type: 'escrow_transfer_operation'
    value: {
      from: string // account_name_type
      to: string // account_name_type
      agent: string // account_name_type
      escrow_id: number // uint32_t
      blurt_amount: {
        amount:    string
        precision: number
        nai:       string
      }
      fee: {
        amount:    string
        precision: number
        nai:       string
      }
      ratification_deadline: string // time_point_sec
      escrow_expiration: string // time_point_sec
      json_meta: string
    }
  }
}

/** Fees paid by an accoun. */
export interface AppliedFeePayOperation extends AppliedOperation {
  op: {
    type: 'fee_pay_operation'
    value: {
      from:   string
      to:     string
      amount: {
        amount:    string
        precision: number
        nai:       string
      }
    }
  }
}

export interface AppliedFillTransferFromSavingOperation extends AppliedOperation {
  op: {
    type: 'fill_transfer_from_savings_operation'
    value: {
      from:       string // account_name_type
      to:         string // account_name_type
      amount:     {
        amount:    string
        precision: number
        nai:       string
      } // VESTS
      request_id: number
      memo:       string
    }
  }
}

export interface AppliedFillVestingWithdrawOperation extends AppliedOperation {
  op: {
    type: 'fill_vesting_withdraw_operation'
    value: {
      from_account: string // account_name_type
      to_account:   string // account_name_type
      withdrawn: {
        amount:    string
        precision: number
        nai:       string
      } // VESTS
      deposited: {
        amount:    string
        precision: number
        nai:       string
      } // BLURT
    }
  }
}

export interface AppliedHardforkOperation extends AppliedOperation {
  op: {
    type: 'hardfork_operation'
    value: {
      hardfork_id: number
    }
  }
}

export interface AppliedProducerRewardOperation extends AppliedOperation {
  op: {
    type: 'producer_reward_operation'
    value: {
      /** The witness account. */
      producer: string // account_name_type
      /** Reward in VESTS. */
      vesting_shares: {
        amount:    string
        precision: number
        nai:       string
      }
    }
  }
}

/** Payment of a proposal */
export interface AppliedProposalPayOperation extends AppliedOperation {
  op: {
    type: 'proposal_pay_operation'
    value: {
      receiver:  string // account_name_type
      payment:   {
        amount:    string
        precision: number
        nai:       string
      } // BLURT
      trx_id:    string
      op_in_trx: number
    }
  }
}

export interface AppliedRecoverAccountOperation extends AppliedOperation {
  op: {
    type: 'recover_account_operation'
    value: {
      /** The account to be recovered. */
      account_to_recover: string // account_name_type
      /** The new owner authority as specified in the request account recovery operation. */
      new_owner_authority: AuthorityType
      /**
       * A previous owner authority that the account holder will use to prove
       * past ownership of the account to be recovered.
       */
      recent_owner_authority: AuthorityType
      /** Extensions. Not currently used. */
      extensions: any[] // extensions_type
    }
  }
}

export interface AppliedRemoveProposalOperation extends AppliedOperation {
  op: {
    type: 'remove_proposal_operation'
    value: {
      proposal_owner: string
      /** IDs of proposals to be removed. Nonexisting IDs are ignored. */
      proposal_ids: number[] // flat_set_ex<int64_t>
      extensions: any[]
    }
  }
}

/**
 * All account recovery requests come from a listed recovery account. This
 * is secure based on the assumption that only a trusted account should be
 * a recovery account. It is the responsibility of the recovery account to
 * verify the identity of the account holder of the account to recover by
 * whichever means they have agreed upon. The blockchain assumes identity
 * has been verified when this operation is broadcast.
 *
 * This operation creates an account recovery request which the account to
 * recover has 24 hours to respond to before the request expires and is
 * invalidated.
 *
 * There can only be one active recovery request per account at any one time.
 * Pushing this operation for an account to recover when it already has
 * an active request will either update the request to a new new owner authority
 * and extend the request expiration to 24 hours from the current head block
 * time or it will delete the request. To cancel a request, simply set the
 * weight threshold of the new owner authority to 0, making it an open authority.
 *
 * Additionally, the new owner authority must be satisfiable. In other words,
 * the sum of the key weights must be greater than or equal to the weight
 * threshold.
 *
 * This operation only needs to be signed by the the recovery account.
 * The account to recover confirms its identity to the blockchain in
 * the recover account operation.
 */
 export interface AppliedRequestAccountRecoveryOperation extends AppliedOperation {
  op: {
    type: 'request_account_recovery_operation'
    value: {
      /** The recovery account is listed as the recovery account on the account to recover. */
      recovery_account: string // account_name_type
      /** The account to recover. This is likely due to a compromised owner authority. */
      account_to_recover: string // account_name_type
      /**
       * The new owner authority the account to recover wishes to have. This is secret
       * known by the account to recover and will be confirmed in a recover_account_operation.
       */
      new_owner_authority: AuthorityType
      /** Extensions. Not currently used. */
      extensions: any[] // extensions_type
    }
  }
}

/**
 * This operation allows recovery_account to change account_to_reset's owner authority to
 * new_owner_authority after 60 days of inactivity.
 */
 export interface AppliedResetAccountOperation extends AppliedOperation {
  op: {
    type: 'reset_account_operation'
    value: {
      reset_account: string // account_name_type
      account_to_reset: string // account_name_type
      new_owner_authority: AuthorityType
    }
  }
}

export interface AppliedReturnVestingDelegationOperation extends AppliedOperation {
  op: {
    type: 'return_vesting_delegation_operation'
    value: {
      account: string // account_name_type
      vesting_shares: {
        amount:    string
        precision: number
        nai:       string
      }
    }
  }
}

/**
 * This operation allows 'account' owner to control which account has the power
 * to execute the 'reset_account_operation' after 60 days.
 */
export interface AppliedSetResetAccountOperation extends AppliedOperation {
  op: {
    type: 'set_reset_account_operation'
    value: {
      account: string // account_name_type
      current_reset_account: string // account_name_type
      reset_account: string // account_name_type
    }
  }
}

/**
 * Allows an account to setup a vesting withdraw but with the additional
 * request for the funds to be transferred directly to another account's
 * balance rather than the withdrawing account. In addition, those funds
 * can be immediately vested again, circumventing the conversion from
 * vests to blurt and back, guaranteeing they maintain their value.
 */
 export interface AppliedSetWithdrawVestingRouteOperation extends AppliedOperation {
  op: {
    type: 'set_withdraw_vesting_route_operation'
    value: {
      from_account: string // account_name_type
      to_account: string // account_name_type
      percent: number // uint16_t (100% = 100_PERCENT = 10000)
      auto_vest: boolean
    }
  }
}

export interface AppliedSpsFundOperation extends AppliedOperation {
  op: {
    type: 'sps_fund_operation'
    value: {
      additional_funds: {
        amount:    string
        precision: number
        nai:       string
      } // BLURT
    }
  }
}

/** Transfers asset from one account to another. */
export interface AppliedTransferOperation extends AppliedOperation {
  op: {
    type: 'transfer_operation'
    value: {
      from:   string
      to:     string
      amount: {
        amount:    string
        precision: number
        nai:       string
      }
      memo:   string
    }
  }
}

export interface AppliedTransferFromSavingsOperation extends AppliedOperation {
  op: {
    type: 'transfer_from_savings_operation'
    value: {
      from: string // account_name_type
      request_id: number // uint32_t
      to: string // account_name_type
      amount: {
        amount:    string
        precision: number
        nai:       string
      }
      memo: string
    }
  }
}

export interface AppliedTransferToSavingsOperation extends AppliedOperation {
  op: {
    type: 'transfer_to_savings_operation'
    value: {
      from: string // account_name_type
      to: string // account_name_type
      amount: {
        amount:    string
        precision: number
        nai:       string
      }
      memo: string
    }
  }
}


/**
 *  This operation converts liquid token (BLURT or liquid SMT) into VFS (Vesting Fund Shares,
 *  VESTS or vesting SMT) at the current exchange rate. With this operation it is possible to
 *  give another account vesting shares so that faucets can pre-fund new accounts with vesting shares.
 */
export interface AppliedTransferToVestingOperation extends AppliedOperation {
  op: {
    type: 'transfer_to_vesting_operation'
    value: {
      from: string // account_name_type
      to: string // account_name_type
      /** must be BLURT or liquid variant of SMT */
      amount: {
        amount:    string
        precision: number
        nai:       string
      }
    }
  }
}

export interface AppliedUpdateProposalVotesOperation extends AppliedOperation {
  op: {
    type: 'update_proposal_votes_operation'
    value: {
      /** IDs of proposals to vote for/against. Nonexisting IDs are ignored. */
      voter: string
      proposal_ids: number[] // flat_set_ex<int64_t>
      approve: boolean
      extensions: any[]
    }
  }
}

/** Vote on a comment. */
export interface AppliedVoteOperation extends AppliedOperation {
  op: {
    type: 'vote_operation'
    value: {
      voter: string
      author: string
      permlink: string
      /**
       * Voting weight, 100% = 10000 (100_PERCENT).
       */
      weight: number
    }
  }
}

export interface AppliedWithdrawVestingOperation extends AppliedOperation {
  op: {
    type: 'withdraw_vesting_operation'
    value: {
      account: string // account_name_type
      /** Amount to power down, must be VESTS. */
      vesting_shares: {
        amount:    string
        precision: number
        nai:       string
      }
    }
  }
}

export interface AppliedWitnessSetPropertiesOperation extends AppliedOperation {
  op: {
    type: 'witness_set_properties_operation'
    value: {
      owner: string
      props: [string, Buffer][]
      extensions: any[]
    }
  }
}

/**
 * Users who wish to become a witness must pay a fee acceptable to
 * the current witnesses to apply for the position and allow voting
 * to begin.
 *
 * If the owner isn't a witness they will become a witness.  Witnesses
 * are charged a fee equal to 1 weeks worth of witness pay which in
 * turn is derived from the current share supply.  The fee is
 * only applied if the owner is not already a witness.
 *
 * If the block_signing_key is null then the witness is removed from
 * contention.  The network will pick the top 21 witnesses for
 * producing blocks.
 */
export interface AppliedWitnessUpdateOperation extends AppliedOperation {
  op: {
    type: 'witness_update_operation'
    value: {
      owner: string // account_name_type
      /** URL for witness, usually a link to a post in the witness-category tag. */
      url: string
      block_signing_key: string | PublicKey | null // public_key_type
      props: WitnessUpdateProperties
      /** The fee paid to register a new witness, should be 10x current block production pay. */
      fee: {
        amount:    string
        precision: number
        nai:       string
      }
    }
  }
}
